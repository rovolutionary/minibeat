(function(root) {
	var App = {};
	
	root.App = App;

	App.resources = {};
	App.views = {};

	App.start = function() {
		start.apply(this, arguments);
	};
	
	function start(opts) {
		_initResources();
		_initViews();
		_bindEvents();
		_initDataPollingWebWorker(opts);
	}

	function _initResources() {
		App.resources.pages = new Pages();
	}

	function _initViews() {
		App.views.pagesListSection = new PagesListSectionView({
			domRef: document.querySelector("#pagesListSection"),
			pages: App.resources.pages
		});
		App.views.referrersListSection = new ReferrersListSectionView({
			domRef: document.querySelector("#referrersListSection")
		});
	}

	function _bindEvents() {
		App.views.pagesListSection.eventManager.on("pageListingClick", function(clickedPageData) {
			App.views.referrersListSection.update(clickedPageData);
		});
	}

	function _initDataPollingWebWorker(opts) {
		var dataPollingWebWorker = new Worker("/js/tasks/poll-api-for-data.js");

		dataPollingWebWorker.addEventListener("message", function(event) {
			var newPageData = event.data;
			App.resources.pages.setData(newPageData);
		});

		dataPollingWebWorker.postMessage({
			pollingRate: opts.pollingRate
		});
	}

})(window);