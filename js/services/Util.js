(function(context) {
	var Util = {};

	context.Util = Util;

	Util.defaults = function() {
		defaults.apply(this, arguments);
	};

	/*
		defaults(object, defaults)
		- This method adds the properties from the 'defaults' object to the 'object' for the properties which are not on 'object'
		- Method returns 'object' agumented with necessary default properties/values
	*/
	function defaults(object, defaults) {
		for(var property in defaults) {
			if(defaults.hasOwnProperty(property)) {
				_addPropertyFromDefaultsToObjectIfNeeded.call(this, property, object, defaults);
			}
		}
		return object;
	}

	function _addPropertyFromDefaultsToObjectIfNeeded(property, object, defaults) {
		if(!object[property]) {
			object[property] = defaults[property];
		}
	}

})(this);