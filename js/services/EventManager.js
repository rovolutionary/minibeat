(function(context) {
	function EventManager() {
		this.eventHandlers = [];
	};

	context.EventManager = EventManager;

	EventManager.prototype.on = function() {
		return on.apply(this, arguments);
	};	

	EventManager.prototype.off = function() {
		return off.apply(this);
	};	

	EventManager.prototype.trigger = function() {
		return trigger.apply(this, arguments);
	};

	EventManager.prototype.cleanup = function() {
		return cleanup.apply(this, arguments);
	};

	function on(_event, _handler, _context) {
		this.eventHandlers.push({
			event: _event,
			handler: _handler,
			context: _context || null
		});

		return this;
	}

	/*	
		off()
		- Unbinds ALL events from manager
	*/
	function off() {
		this.eventHandlers.forEach(function(eventHandler) {
			delete eventHandler.event;
			delete eventHandler.handler;
			delete eventHandler.context;
			eventHandler = null;
		});
		this.eventHandlers = [];

		return this;	
	}

	function trigger(event) {
		var eventTriggerArguments = Array.prototype.slice.call(arguments, 1);
		
		this.eventHandlers.forEach(function(eventHandler) {
			if(eventHandler.event === event) {
				eventHandler.handler.apply(eventHandler.context, eventTriggerArguments);
			}
		});

		return this;
	}

	function cleanup() {
		off.call(this);
		this.eventHandlers = null;
	}

})(this);