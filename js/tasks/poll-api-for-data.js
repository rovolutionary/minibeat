importScripts(
	'../services/Util.js', 
	'../services/EventManager.js', 
	'../resources/Pages.js'
);

(function() {
	var pollingRate,
		pages = new Pages({
			url: "http://api.chartbeat.com/live/toppages/v3/",
			params: {
				limit: 10,
				host: "avc.com",
				apikey: "317a25eccba186e0f6b558f45214c0e7"
			}
		});

	this.addEventListener("message", function(event) {
		pollingRate = event.data.pollingRate || 5000;
		pages.fetch();
		pages.eventManager.on("update", function() {
			this.postMessage(pages.data);
			_pollForNewData();
		});
	});

	function _pollForNewData() {
		setTimeout(function() {
			pages.fetch();
		}, pollingRate);
	}

})();