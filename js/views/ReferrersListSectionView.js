(function(context) {
	var defaultOptions = {
			domRef: null
		};

	function ReferrersListSectionView(options) {
		this.options = options || {};
		Util.defaults(this.options, defaultOptions);
		this.refererViews = [];
		this.listView = _initListView.call(this);
		_cacheDOMRefs.call(this);
	};

	context.ReferrersListSectionView = ReferrersListSectionView;

	ReferrersListSectionView.prototype.update = function() {
		update.apply(this, arguments);
	}

	function update(pageData) {
		_setPageTitle.call(this, pageData.title);
		this.listView.update(pageData.stats.toprefs);
	}

	function _initListView() {
		var newListView = new ListView({
			domRef: this.options.domRef.querySelector("#referrersList"),
			ListingView: RefererListingView
		});
		
		return newListView;
	}

	function _setPageTitle(newPageTitle) {
		this.cachedDOMRefs.pageName.innerText = newPageTitle;
	}

	function _cacheDOMRefs() {
		this.cachedDOMRefs = {};
		this.cachedDOMRefs.pageName = this.options.domRef.querySelector("#referrersListPageName");
		this.cachedDOMRefs.referersList = this.options.domRef.querySelector("#referrersList");
	}
	
})(this);