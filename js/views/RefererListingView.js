(function(context) {
	var pageListingTemplate = document.querySelector("#refererListingTemplate").innerHTML,
		defaultOptions = {
			data: null
		};

	function RefererListingView(options) {
		this.options = options || {};
		Util.defaults(this.options, defaultOptions);
		this.domRef = _createView.call(this);
		return this;
	};

	context.RefererListingView = RefererListingView;

	RefererListingView.prototype.cleanup = function() {
		cleanup.apply(this);
	};

	function cleanup() {
		this.domRef = null;
		this.options = null;
	}

	function _createView() {
		var pageListingElem = document.createElement("li");

		pageListingElem.innerHTML = pageListingTemplate;
		pageListingElem.querySelector(".referer-current-visitors-number").textContent = this.options.data.visitors;
		pageListingElem.querySelector(".referer-name").textContent = this.options.data.domain;

		return pageListingElem;
	}
	
})(this);