(function(context) {
	var clickedPageListingClassTag = "clicked-page-listing",
		defaultOptions = {
			domRef: null,
			pages: null
		};

	function PagesListSectionView(options) {
		this.options = options || {};
		Util.defaults(this.options, defaultOptions);
		this.pageViews = [];
		this.eventManager = new EventManager();
		this.lastClickedPageListingView = null;
		this.listView = _initListView.call(this);
		_bindEventHandlers.call(this);
	};

	context.PagesListSectionView = PagesListSectionView;

	function _bindEventHandlers() {
		this.options.pages.eventManager.on("update", _updateListView, this);
		this.listView.eventManager.on("listingClicked", _triggerPageListingClickedEvent, this);
		this.listView.eventManager.on("listingClicked", _setLastClickedPageListingView, this);
	}

	function _initListView() {
		var newListView = new ListView({
			domRef: this.options.domRef.querySelector("#pagesList"),
			ListingView: PageListingView,
			eventsToListenFor: ["listingClicked"]
		});
		
		return newListView;
	}

	function _updateListView() {
		this.lastClickedPageListingView = null;
		this.listView.update(this.options.pages.data);
	}

	function _triggerPageListingClickedEvent(clickedPageListingView) {
		this.eventManager.trigger("pageListingClick", clickedPageListingView.data);
	}

	function _setLastClickedPageListingView(clickedPageListingView) {
		var clickedPageListingAlreadySelected = this.lastClickedPageListingView === clickedPageListingView;
		
		if(!clickedPageListingAlreadySelected) {
			if(this.lastClickedPageListingView) {
				this.lastClickedPageListingView.domRef.classList.remove(clickedPageListingClassTag);
			}
			clickedPageListingView.domRef.classList.add(clickedPageListingClassTag);
			this.lastClickedPageListingView = clickedPageListingView;	
		}
	}
	
})(this);