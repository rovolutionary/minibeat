(function(context) {
	var defaultOptions = {
		domRef: null,
		eventsToListenFor: [],
		ListingView: null
	};

	function ListView(options) {
		this.options = options || {};
		Util.defaults(this.options, defaultOptions);
		this.listingViews = [];
		this.eventManager = new EventManager();
	};

	context.ListView = ListView;

	ListView.prototype.update = function() {
		update.apply(this, arguments);
	}

	function update(listData) {
		_removeAllListViews.call(this);
		_renderListViews.call(this, listData);
	}

	function _removeAllListViews() {
		var _this = this;

		_this.listingViews.forEach(function(listingView) {
			_this.options.domRef.removeChild(listingView.domRef);
			listingView.cleanup();
			listingView = null;
		});
		_this.listingViews = [];
	}

	function _renderListViews(listViewData) {
		var _this = this,
			newListingsDocFrag = document.createDocumentFragment();

		listViewData.forEach(function(listingData) {
			var newRefererListingView = _createListingView.call(_this, listingData);
			newListingsDocFrag.appendChild(newRefererListingView.domRef);
		});
		_this.options.domRef.appendChild(newListingsDocFrag);
	}

	function _createListingView(listingData) {
		var newListingView = new this.options.ListingView({
			data: listingData
		});

		if(this.options.eventsToListenFor.length) {
			_bindEventHandlersToListing.call(this, newListingView);
		}
		this.listingViews.push(newListingView);

		return newListingView;
	}

	function _bindEventHandlersToListing(newListingView) {
		var listingClickedEventIsBeingListenedFor = _isEventBeingListenedFor.call(this, "listingClicked");
		
		if(listingClickedEventIsBeingListenedFor) {
			newListingView.eventManager.on("clicked", _triggerListingClickedEvent, this);
		}
	}

	function _isEventBeingListenedFor(event) {
		return this.options.eventsToListenFor.some(function(evt) {
			return evt === event;
		});
	}

	function _triggerListingClickedEvent(clickedListing) {
		this.eventManager.trigger("listingClicked", clickedListing);
	}
	
})(this);