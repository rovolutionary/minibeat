(function(context) {
	var pageListingTemplate = document.querySelector("#pageListingTemplate").innerHTML,
		pageListingClassTag = "page-listing",
		defaultOptions = {
			data: []
		};

	function PageListingView(options) {
		this.options = options || {};
		Util.defaults(this.options, defaultOptions);
		this.data = this.options.data;
		this.domRef = _createView.call(this);
		this.domEventHandlers = [];
		this.eventManager = new EventManager();
		_bindEventHandlers.call(this);
		return this;
	};

	context.PageListingView = PageListingView;

	PageListingView.prototype.cleanup = function() {
		cleanup.apply(this);
	};

	function cleanup() {
		_unbindEventHandlers.call(this);
		this.eventManager.cleanup();
		this.domRef = null;
		this.options = null;
		this.data = null;
		this.eventManager = null;
	}

	function _createView() {
		var pageListingElem = document.createElement("li");

		pageListingElem.innerHTML = pageListingTemplate;
		pageListingElem.classList.add(pageListingClassTag);
		pageListingElem.querySelector(".page-current-visitors-number").textContent = this.data.stats.people;
		pageListingElem.querySelector(".page-name").textContent = this.data.title;

		return pageListingElem;
	}

	function _bindEventHandlers() {
		var triggerListClickedEventFromPageListingContextFn = _triggerListClickedEvent.bind(this);
		
		_addDOMEventHandler.call(this, "click", triggerListClickedEventFromPageListingContextFn);
	}

	function _triggerListClickedEvent() {
		this.eventManager.trigger("clicked", this);
	}

	function _addDOMEventHandler(type, handler) {
		this.domRef.addEventListener(type, handler);
		this.domEventHandlers.push({
			"type": type,
			"handler": handler
		});
	}

	function _unbindEventHandlers() {
		var _this = this;
		
		_this.domEventHandlers.forEach(function(eventHandler) {
			_this.domRef.removeEventListener(eventHandler.type, eventHandler.handler);
			delete eventHandler.type;
			delete eventHandler.handler;
			eventHandler = null;
		});
		_this.domEventHandlers = null;
	}
	
})(this);