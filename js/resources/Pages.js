(function(context) {
	var defaultOptions = {
		url: "http://api.chartbeat.com/live/toppages/v3/",
		params: {
			limit: 10,
			host: "avc.com",
			apikey: "317a25eccba186e0f6b558f45214c0e7"
		}
	};

	var Pages = function(options) {
		this.options = options || {};
		Util.defaults(this.options, defaultOptions);
		this.data = [];
		this.eventManager = new EventManager();
	};

	context.Pages = Pages;

	Pages.prototype.fetch = function() {
		fetch.apply(this);
	};

	Pages.prototype.setData = function() {
		setData.apply(this, arguments);
	};

	function fetch() {
		var _this = this,
			request = _createAndSendHttpRequest.call(this),
			requestSuccessCallback = _requestSuccessCallback.bind(this, request),
			requestFailureCallback = _requestFailureCallback.bind(this, request);

		request.addEventListener("load", requestSuccessCallback);
		request.addEventListener("error", requestFailureCallback);
	}

	function setData(newData) {
		if(newData instanceof Array) {
			this.data = newData;
			this.eventManager.trigger("update");
		} else {
			throw new Error("'data' for Pages resource must be an Array");
		}
	}

	function _requestSuccessCallback(request) {
		if(request.status === 200) {
			this.data = JSON.parse(request.response).pages;
			this.eventManager.trigger("update");
		}
	}

	function _requestFailureCallback(request) {
		this.eventManager.trigger("error", request);
	}

	function _createAndSendHttpRequest() {
		var request = new XMLHttpRequest(),
			params = JSON.stringifyparams,
			formattedRequestURL = _formatURL(this.options.url, this.options.params);

		request.open('GET', formattedRequestURL);
		request.send();

		return request;
	}

	function _formatURL(url, params) {
		var requestURL = url;

		if(params) {
			var paramsString = _createParamsString(params);
			requestURL += "?" + paramsString;
		}

		return encodeURI(requestURL);
	}

	function _createParamsString(params) {
		var paramsString = "";

		Object.keys(params).forEach(function(param, index) {
			var paramValue = params[param],
				singleParamString = param + "=" + paramValue;

			if(index === 0) {
				paramsString += singleParamString;
			} else {
				paramsString += ("&" + singleParamString);
			}
		});

		return paramsString;
	}

})(this);